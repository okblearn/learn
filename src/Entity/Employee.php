<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Сотрудник
 *
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 */
class Employee
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Имя сотрудника
     *
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * Фамилия сотрудника
     *
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * E-mail сотрудника
     *
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * Пароль
     * TODO: Потом будет хэш и вообще нормальная авторизация
     *
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\ManyToOne(targetEntity="Department", inversedBy="employees")
     */
    private $department;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(Department $department): self
    {
        $this->department = $department;

        return $this;
    }
}

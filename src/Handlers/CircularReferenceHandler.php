<?php
namespace App\Handlers;

class CircularReferenceHandler
{
    public function __invoke($object)
    {
        return $object->getId();
    }
}

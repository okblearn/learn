<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Контроллер по умолчанию
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", methods="GET", name="default")
     */
    public function index(): Response
    {
        return $this->render('default.html.twig', []);
    }

}

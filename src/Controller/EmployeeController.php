<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Form\EmployeeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Контроллер сотрудников
 */
class EmployeeController extends AbstractController
{
    /**
     * Список всех сотрудников
     *
     * @Route("/employee", methods="GET", name="employees")
     */
    public function index(): Response
    {
        $employees = $this->getDoctrine()->getRepository(Employee::class)->findAll();
        return $this->json($employees);
    }

    /**
     * Получить сотрудника по id
     * Название метода getEmployee, т.к. get уже занято
     *
     * @Route("/employee/{id}", methods="GET", name="employee_get")
     * @param Employee $employee
     * @return Response
     */
    public function getEmployee(Employee $employee): Response
    {
        return $this->json($employee);
    }

    /**
     * Добавить нового сотрудника
     *
     * Данные должны быть вида:
     * employee[firstName] => Петр
     * employee[lastName] => Петров
     * employee[email] => petr@test.com
     * ...
     *
     * @Route("/employee", name="employee_add", methods="POST")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $employee = new Employee();
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($employee);
            $em->flush();

            return $this->json($employee);
        }

        return $this->json(false);
    }

    /**
     * Изменение сотрудника
     * Данные как в add
     *
     * @Route("/employee/{id}", name="employee_update", methods="POST")
     * @param Request $request
     * @param Employee $employee
     * @return Response
     */
    public function update(Request $request, Employee $employee): Response
    {
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->json($employee);
        }

        return $this->json(false);
    }

    /**
     * Удаление сотрудника
     *
     * @Route("/employee/{id}", name="employee_delete", methods="DELETE")
     * @param Employee $employee
     * @return Response
     */
    public function delete(Employee $employee): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($employee);
        $em->flush();

        return $this->json(true);
    }

}

import React from 'react';

/**
 * Компонент списка сотрудников
 */
class List extends React.Component {
    constructor() {
        super();
        this.state = {employees: []};
    }

    componentDidMount() {
        $.get('/api/v1/employee', function(employees){
            this.setState({employees: employees});
        }.bind(this));
    }

    render() {
        let list = this.state.employees.map((employee) => {
            return <tr key={employee.id}><td>{employee.firstName} {employee.lastName}</td><td>{employee.email}</td></tr>
        });

        return <table>
            <thead>
                <tr>
                    <th>Имя</th><th>E-mail</th>
                </tr>
            </thead>
            <tbody>
                {list}
            </tbody>
        </table>;
    }
}

export default List;

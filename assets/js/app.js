require('../css/app.css');
import $ from 'jquery';

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
//var $ = require('jquery');

//console.log('Hello Webpack Encore! Edit me in assets/js/app.js');

import React from 'react';
import ReactDOM from 'react-dom';
import List from './Components/Employee/List';

class App extends React.Component {
    constructor() {
        super();
    }

    render() {
        return <div>
            <List/>
        </div>;
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
